---
layout: markdown_page
title: "Product Vision - CI/CD"
---

- TOC
{:toc}

## Overview

Our vision for CI/CD is aligned to the one set forth in Jez Humble's classic [Continuous Delivery](https://continuousdelivery.com/) book.
The ideas of reducing risk, getting features and fixes out quickly, and reducing costs/creating happier teams by removing the barriers
to getting things done has stood the test of time. 

The CI/CD section focuses on the code build/verification ([Verify](/direction/verify)), packaging/distribution
([Package](/direction/package)), and delivery ([Release](/direction/release)) stages of the
[DevOps Lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). Each of these areas has their own strategy
page with upcoming features, north star directions, and more. This page ties them together via important concepts
that unify the direction across all of these areas. In addition to the core CI/CD elements, the 
[mobile use case](/direction/mobile) is very much related and will be of interest to you if you are interested
in building and releasing mobile applications using GitLab.

If you'd like to discuss this vision directly with the product director for CI/CD, 
feel free to reach out to Jason Lenny via [e-mail](mailto:jason@gitlab.com) or on [Twitter](https://twitter.com/j4lenn).
Your contribution is more than welcome.

You can see how the CI/CD stages relate to each other in the following infographic:

![Pipeline Infographic](https://docs.gitlab.com/ee/ci/introduction/img/gitlab_workflow_example_extended_11_11.png "Pipeline Infographic")

## Theme: Compliance as Code

Many in the industry talk about compliance as code (i.e., methods that ensure the correct regulatory or company
compliance requirements) as a state where requirements are fulfilled with zero-touch on the path to production.
We believe that [compliance without friction](https://about.gitlab.com/solutions/compliance/) is a crucial
solution for large enterprises and those subject to complex regulation, and that GitLab is well suited to
provide a comprehensive solution.

At the moment we are only scratching the surface for what customers need to achieve minimally viable compliance. 
However, our vision for a single application - integrating not only Secure, Verify, Package and Release but the entire
DevOps lifecycle - will uniquely position GitLab to have the ability to be the number one market leader in
built-in compliance, for users big or small. Beyond just providing places to plug-in your own compliance
process, we have a vision for compliance best-practices being delivered through GitLab.

The primary home for this theme is our [Release Governance](https://about.gitlab.com/direction/release/release_governance/)
category.

## Theme: Progressive Delivery

Put simply, Progressive Delivery is a core set of ideas and emerging best practices, oriented around
being able to control and monitor deployments in stages over time and in an automated and safe
way. GitLab is at a unique advantage when it comes to enabling this because, until now, Continuous
Delivery solutions have often required integrating various point products to provide each of the
individual capabilities needed. Here at GitLab, we're going to be implementing and
integrating these features within our single application, with Progressive Delivery in mind from the start.

Additional articles on Progressive Delivery can be found on the [LaunchDarkly blog](https://launchdarkly.com/blog/progressive-delivery-a-history-condensed/),
[RedMonk](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/) and 
[The New Stack](https://thenewstack.io/the-rise-of-progressive-delivery-for-systems-resilience/).

[Release Orchestration](https://about.gitlab.com/direction/release/release_orchestration/) and [Continuous Delivery](https://about.gitlab.com/direction/release/continuous_delivery/) are
our core categories for Progressive Delivery, but there are additional ones that will be important
in providing our comprehensive solution:

- [Feature Flags](https://about.gitlab.com/direction/release/feature_flags/) for targeted rollouts
- [Review Apps](https://about.gitlab.com/direction/release/review_apps/) for on-demand validation environments
- [Tracing](https://gitlab.com/groups/gitlab-org/-/epics/89) for behavior analysis across progressive deployments

We truly believe Progressive Delivery is the future of software delivery, and we plan to deliver an MVC
solution soon: [gitlab-org#1198](https://gitlab.com/groups/gitlab-org/-/epics/1198)

## Theme: Powerful, Integrated Primitives

One major challenge with CI/CD solutions is that it is difficult to maintain a powerful set of capabilities, but in
an expressive language and implementation that does not introduce complexity to your configuration before you need it. 
We have identified a few simple but powerful primitives to add to GitLab CI/CD which will allow us to achieve our goal.
Each of these works independently to solve problems from simple to advanced, but truly become powerful when used in concert.

If you're interested in learning more, check out this [deep dive video](https://youtube.com/embed/FURkvXiiJek) where we discuss in detail how these ideas related to each other, or [this one](https://youtu.be/XxFX2FOp83A) in which we discuss sequencing options.

### Foundation

In terms of building out capabilities for CI/CD pipelines, there are a couple key places to start that more advanced
features can build on.

**Better framework for concurrent, namespaced execution**: 
As complexity in pipelines grows, you start to run into problems with job/variable name or other namespace collisions. Templates
are important, but also contribute to this complexity. Furthermore, with monorepo and microservices architectures you often
end up needing a lot of parallel execution paths that don't have much to do with each other other than needing to come together
at the end; under the current stage-based parallellization and sequencing model, these become much more bound to each other than
they really need to be.

In order to provide a better solution for these use cases, our plan is to implement
[Multiple Pipelines in a single .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972).
This feature will allow you to trigger independent child pipelines from a single pipeline run, each of which runs independently, under
its own namespace, and provides status attribution back to the parent. These pipelines will also follow `only`/`except` criteria,
allowing them to only be triggered based on certain scenarios such as changes to a path or in the context of an MR.

**Workspaces for multi-step pipelines that transform a working set**:
A very common use case for pipelines is to implement a series of jobs that perform a transformation and/or analysis on a working set
of files. Most builds work this way essentially, but these are becoming even more common as a paradigm for data science models.
At the moment GitLab provides artifacts as one option, but these are more meant for longer term storage. We also provide
caching, but the cache is best-effort (so not guaranteed) and can cause surprises if its used as a workspace.

With [gitlab-ce#47062](https://gitlab.com/gitlab-org/gitlab-ce/issues/47062) we will allow for persisting a workspace from job to
job. Simply let GitLab know in your `.gitlab-ci.yml` that intermediary output is produced in your pipeline, and we'll handle
automatically ensuring that downstream jobs get access to upstream outputs.

### What Comes After

Once our foundation is in place, there are a few different directions we can go that we're researching.

**Dependency-Based Pipeline Execution**:
The combination of workspaces and parent/child pipelines provides a solution for a lot of use cases, but one that's remaining
is the idea that you can define dependencies between jobs and GitLab itself can figure out the fastest way to run the pipeline.
One way to do this is through a [Directed Acyclic Graph (DAG)](https://gitlab.com/gitlab-org/gitlab-ce/issues/47063), where
through a keyword like `needs:` you let us know which jobs depend on which. Then, we can organize everything as parent/child
pipelines to execute in the fastest way possible.

**Namespaces for Includes**:
By enabling parent/child pipelines we delay the likelihood of conflicts between templates, but aren't eliminating it. By
introducing actual namespacing for includes we could provide containers for references which would allow for true assurance
that there will not be any conflicts, similar to what a programming language might provide for importing libraries. This can
add significant complexity to configuration, but may become necessary.

**Pipeline Entity Model**:
Pipelines today can internally track a global success/failure status and generate artifacts, but don't have an inbuilt way to
reflect on their own state and output, or the states and outputs of other jobs/pipelines they may be aware of. Adding this ability
would allow for advanced behaviors of pipelines - for example, you could build a job that only triggers when a separate job (or any
job) produces a file called `error.out` and which is responsible for handling the contents of that file, or you could set a pipeline to
run if another pipeline anywhere on the system fails.

**Templating/Generation options for `.gitlab-ci.yml`**:
In general, configuration syntax can be unwieldy. We have issues like [gitlab-ce#19199](https://gitlab.com/gitlab-org/gitlab-ce/issues/19199)
that allow for a cartesian product of configurations to be defined, without having to define each job in the matrix (i.e., likely a list of 
architecture or platform targets.) We also have issues for potential integration with technologies like JSonnet
([gitlab-ce#62456](https://gitlab.com/gitlab-org/gitlab-ce/issues/62456)) which would allow for templating of configuration. Another
step beyond this could be [gitlab-ce#45828](https://gitlab.com/gitlab-org/gitlab-ce/issues/45828), which will allow for custom bootstrap code
to be inserted that would then generate your `.gitlab-ci.yml`.

**Auto-Inclusion of Sub-Pipeline Configuration**:
There's another nice case for monorepos, for example, where you may just want to place `.gitlab-ci.yml` files throughout your repo
and these are automatically evaluated for potential sub-pipeline inclusion, without having to have a master list of sub-pipelines
centrally managed. We can potentially include this, but this also could result in surprising behaviors so it's something we'd want to
carefully consider.

## What's Next for CI/CD

<%= direction %>
