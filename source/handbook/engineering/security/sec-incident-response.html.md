---
layout: markdown_page
title: "GitLab Security Incident Response Guide"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab Security Incident Response Guide

This living guide is intended to explain to users the why, when, and how of security incident response at GitLab.

## Engaging Security Operations

If an urgent security incident has been identified or you suspect an incident may have occurred, Security Operations on-call can be paged by:

* Slack: Use the command `/security <issue description>` in GitLab Slack
* Email: Send an email with a description of the incident to `page-security@gitlab.com`

For lower severity requests or general Q&A, GitLab Security is available in the `#Security` channel in GitLab Slack and the Security Operations team can be alerted by mentioning `@sec-ops-team`. If you suspect you've received a phishing email please see: [What to do if you suspect an email is a phishing attack](https://about.gitlab.com/handbook/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack).

## Incident Identification

Security incident investigations are conducted when a security incident has been detected on [GitLab.com](https://www.gitlab.com), regardless of whether it's a single user or multiple projects, and these investigations are handled with the same level of urgency and priority. Indicators can be reported to us either internally, by a GitLab team member, or externally. It is the Security team's responsibility to determine when to investigate, dependent on the identification and verification of a security incident. The GitLab Security team identifies security incidents as any violation, or threat of violation, of GitLab security, acceptable use or other relevant policies.

## Confidentiality

Security incidents may (and usually do) involve sensitive information related to GitLab, GitLab's customers or employees, or users who (in one way or another) have engaged with GitLab.  GitLab, while codifying the [Transparency](https://about.gitlab.com/handbook/values/#transparency) value, also strongly believes in and strives to maintain the privacy and confidentiality of the data its' employees, customers, and users have entrusted us with.

A **confidential** issue means any data within the issue and any discussions about the issue or investigation are to be kept to **GitLab employees only** unless permission is explicitly granted by GitLab Legal, GitLab Security Director, or the GitLab Executive Team.

## Incident Tracking

Security incident investigations must begin by opening a tracking issue in the [Security Operations](https://gitlab.com/gitlab-com/gl-security/operations/issues) project.  This tracking issue will be the primary location where all work and resulting data collection will reside throughout the investigation. All artifacts from an investigation must be handled per the [Artifact Handling and Sharing](https://gitlab.com/gitlab-com/gl-security/runbooks/blob/master/security_operations/handling_and_sharing_artifacts.md) internal only runbook.

**NOTE:** The tracking issue, any collected data, and all other engagements involved in a Security Incident must be kept **strictly confidential**.

## Incident Severity

Assigning severity to an incident isn't an exact science and it takes some rational concepts mixed with past experiences and gut feelings to decide how bad a situation may be.  When considering severity, look at:

* The type of data involved and how it's classified using the [Data Classification Policy](./data-classification-policy.html)
  * Was this data leaked or disclosed to parties who should not have visibility to it?
  * Was the data been modified in our records? (either confirmed or believed to be)
* Was a user or service account taken over?
  * What level of access did this account have and to what services or hosts?
  * What actions were taken by the compromised account?
* If a vulnerability is present on a host or service, consider the impact it might have on GitLab and the likelihood of it being exloited by using the [Risk Matrix](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/Risk%20Scoring%20Matrix.md). (Internal Only)
  * Was the vulnerability exploited? If so, how was it used and how frequently?
* What is the scope of the incident?
  * How many GitLab.com users were/may have been impacted?
  * How many hosts or services?
* Has this incident resulted in any hosts or services being unavailable?

After taking these types of questions into consideration, review the [Overall Impact](https://gitlab.com/gitlab-com/gl-security/compliance/risk-assessments/blob/master/Risk%20Scoring%20Matrix.md#overall-impact) to help place a severity rating on the incident.

## Communication

* Invite all available parties to the SecOps incident response Zoom conference bridge for easier discussion (see topic in the SecOps slack channel)
* Open an incident-focused Slack channel to centralize non-verbal discussion, particularly if the incident is of a sensitive nature. This should follow the naming convention `#secops_####` where #### is the GitLab issue number in the Security Operations project.
* Create a shared Google Doc to act as a centralized record of events in real-time.  Try to capture significant thoughts, actions, and events as they're unfolding.  This will simplify potential hand-off's and eventual RCA of the incident.
* Setup a [shared Google Drive folder](https://gitlab.com/gitlab-com/gl-security/runbooks/blob/master/security_operations/handling_and_sharing_artifacts.md#storing-and-sharing-files-using-google-cloud-storage) for centralized storage of evidence, data dumps, or other pieces of critical information for the incident.

[Data Breach Notification Policy](https://about.gitlab.com/security/#data-breach-notification-policy)

## Containment

Once an incident has been identified and its severity set, the incident responder must attempt to limit the damage that has already occurred and prevent any further damage from occurring. The first step in this process is to identify impacted resources and determine a course of action to contain the incident while potentially also preserving evidence. The containment strategy will vary based on the type of incident being responded to but can be as simple as marking an issue confidential to stop an information disclosure or to block access to a network segment. It is important to remember that the containment phase of incident response is typically a short-term solution to limit damage and not to produce a long term fix for the underlying problem. Additionally the impact of the mitigation on the service must be weighed against the severity of the incident.
