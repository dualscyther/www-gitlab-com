---
layout: markdown_page
title: "VUL.2.01 - Application Penetration Testing Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# VUL.2.01 - Application Penetration Testing

## Control Statement

GitLab conducts penetration tests according to the service risk rating assignment.

## Context

This control is meant to formalize the way GitLab prioritizes our penetration tests. The rating assignment mentioned in this control is detailed in a separate control linked below. It isn't feasible to test 100% of GitLab systems and since penetration tests are meant to reduce risk to the organization, it makes sense that risk is the method we use for prioritizing which systems we test in a given year.

## Scope

This control applies to penetration testing performed against any GitLab production systems.

## Ownership

The GitLab security team manages the penetration testing process and associated risk rating.

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.2.01_application_penetration_testing.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.2.01_application_penetration_testing.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/VUL.2.01_application_penetration_testing.md).

## Framework Mapping

* ISO
  * A.12.6.1
* SOC2 CC
  * CC7.1
* PCI
  * 11.3
  * 11.3.1
  * 11.3.2
  * 11.3.4
