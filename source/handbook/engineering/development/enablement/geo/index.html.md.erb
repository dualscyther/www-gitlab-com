---
layout: markdown_page
title: "Geo and Disaster Recovery"
description: "Summary of how the Geo Team operates"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## The Geo Team

[Geo](/features/gitlab-geo/) is a [Premium](/pricing/) feature, built to help speed up the development of distributed teams by providing
one or more read-only mirrors of a primary GitLab instance. This mirror (a Geo secondary node) reduces the time to clone or fetch large
repositories and projects, or can be part of a Disaster Recovery solution.

### Team members

<%= direct_team(manager_role: 'Engineering Manager, Geo') %>

### Stable counterparts

<%= stable_counterparts(role_regexp: /[,&] Geo/, direct_manager_role: 'Engineering Manager, Geo') %>

## Goals and Priorities

Our priorities are aligned with the product direction. You can read more about this on the [Geo Product Vision page](https://about.gitlab.com/direction/geo/).

Alongside the items listed in our Product Vision, we need to constantly assess issues that our customers bring to our
attention. These could take the form of bug reports or feature requests. Geo users are often our largest
customers and some rely on Geo as a critical part of their workflow.

We also work constantly to keep housekeeping chores to a manageable level. Where possible, we address these issues
as part of a related project. Where this is not possible, we use time around our projects to make this happen.

## Geo's Relationship to Disaster Recovery

Disaster Recovery (DR) is a set of policies, tools and procedures put in place to be able to recover from a disaster. 

Geo provides data redundancy. The customer will have a redundant copy of data in a separate location. If anything were to happen to their primary instance, a secondary instance still retains a copy of the data. 

However, data redundancy is one part of a complete DR strategy. 

High Availability (HA) is also a step towards Disaster Recovery. At the moment Geo does not provide true HA because if the primary instance is not available, certain actions are not possible.

## Common Links

Documentation
- [Geo](https://docs.gitlab.com/ee/gitlab-geo/)
- [Disaster Recovery](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/index.html)
- [Planned Failover](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/planned_failover.html)
- [Background Verification](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/background_verification.html)

Other Resources
- Issues relating to Geo are mostly to be found on the
[gitlab-ee issue tracker](https://gitlab.com/gitlab-org/gitlab-ee/issues/?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo)
- [Chat channel](https://gitlab.slack.com/archives/g_geo); please use the `#g_geo`
chat channel for questions that don't seem appropriate to use the issue tracker
for.

## Geo Terminology

| Term  |  Definition |  
|---|---|
| Geo |  The product name given to the feature that provides the ability to create one or more read-only mirrors for the main/primary instance |
| Primary  | The main, primary instance where read-write operations are allowed |
| Secondary  | An instance that synchronizes with the Primary node where only read-only operations are permitted |

## Planning and Demos

### Discussions

Discussions are documented [separately](https://docs.google.com/document/d/18vGk6dQs7L0oGQOb_bNiFa5JhwLq5WBS7oNxQy09ml8/edit#heading=h.p295wb40mdh4).

### Planning

[Here](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Geo) you can find the Epics for Geo.

#### Epic Ownership

The Geo team uses epics to describe features or capabilities that will increase the maturity of the Geo categories over time.

Each Epic should be owned by an engineer who is responsible for all technical aspects of that Epic. If at any point, the
owner needs to take leave that is more than a few days, they should assign another engineer to act as owner until they return.

**In the planning phase**, the engineering owner will work closely with the Product Manager to understand what the requirements
are and why they are important to customers. The engineer will decide on how best to deliver the solution and create issues
that encapsulate the technical work required. They may need to consult with other team members and stable counterparts
to come up with the right approach to delivering the requirements.

They should include issues for documentation changes and additional testing requirements that are required. There should
be an issue created to inform Geo experts in the Support group about the change if needed. The engineer
should also consider if there is any technical debt that is appropriate to address at the same time. Issues are also
needed for any rollout or post-release tasks.

Each issue needs to be weighted and contain enough information for any other engineer on the team to be able
to pick up that work.

When they are satisfied that all of the issues are created, and ready for development, they (with the PM) should present
this information to the team for feedback. Preparation is still transparent and open to feedback at any point, this step
is just a way to tell the team that the Epic is ready to be built and confirm that everyone is aware of what needs to be done.

**For the duration of building the epic**, the engineer does not need to be the only person implementing the issues.
They should keep watch of the work that is done on the issues so that they can verify that the work is progressing correctly.
Ideally, they should also be an approver on each MR for the Epic. If there are problems with the work, or lengthy delays,
they need to make sure the Product Manager and Engineering Manager are aware.

**When work is nearing completion**, the engineer should check the release note and work with the PM on any changes.
They should also make sure that any additional issues that may have come up during the build process are either
addressed, or scheduled for work. This will help to make sure that we do not build up technical debt while building.

**Finally**, they should also monitor any work that needs to occur while rolling out the Epic in production. If there are
rake tasks, database migrations, or other tasks that need to be run, they need to see those through to being
run on the production systems with the help of the Site Reliability counterpart. They may also need to assist the
Product Manager with creating the release note for the Epic.

This places a lot of responsibility with the owner, but the PM and EM are always there to support them. This ownership
removes bottlenecks and situations where only the PM or EM is able to advance an idea. In addition, the best people
to decide on how to implement an issue are often the people who will actually perform the work.

The responsibility of the Product Manager is to make sure that there is enough information for the engineer to develop the
correct solution that meets the requirements. They are also available to answer clarifying questions or consider how to
approach edge cases. At the end of an epic they also communicate this out to customers and other interested parties.

The Engineering Manager is responsible for clearing the path. They need to make sure the engineers performing the work
have the access to the right information, people, tools, and other resources to get the work done. They try to foresee
problems and clear any blockers that may arise while the work is in progress.

#### Milestones

New milestones will contain the current work-in-progress issues from the previous milestone, along with any issues labelled as "Deliverable" for that Milestone.

A planning issue is created for each Milestone in the [geo-team project](https://gitlab.com/groups/geo-team/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Milestone%20Planning).
[This is an example of a plannning issue](https://gitlab.com/geo-team/planning-discussions/issues/6).

The team is tagged in the issue and all team members will read through the issues to get an overview
of the deliverables planned, along with the estimated weight. If anyone has a concern about the weight of an issue, they are expected to start a discussion on the issue
to confirm that there are no misunderstandings about the requirements. This was the simplest approach we could find to collaborate on assigning weight to issues.

Please assign at least one deliverable to yourself before the start of the milestone. Any unassigned deliverables will be allocated to the most appropriate engineer by
the engineering manager before the milestone begins.

When the work-in-progress from the previous milestone and the deliverables are completed, please choose new issues from the top of the "ready for development" column on the [Geo Backlog board](https://gitlab.com/groups/gitlab-org/-/boards/981056?milestone_title=No+Milestone&&label_name[]=Geo).

##### Addressing new issues quickly

When new issues arise (through testing, customer support issues, or other means) we still want to be able to address them quickly without being delayed by process.
If you think a new issue needs to be worked on immediately:

1. Make sure there is enough detail on the ticket for someone else to understand the issue and for someone else to have enough context to review your work
1. Make sure there is a weight
1. Assign it into the current milestone

#### Weights

We use the weights from the set [1, 3, 5, 10, 20] where the value represents a "best work day". A "best work day" means a day where there are no interruptions, emails or other demands on your time.
For example, if an issue is assigned a weight of 5 then an engineer should be able to complete the work in 5 days if this were the only thing that they needed to do for those five days.

If an issue is assigned a weight of 20, then the issue is too big and will need to be broken down further. Issues weighted at 10 will also need a rough to-do list added to the issue. (An issue of this size
probably doesn't need multiple related issues, but a to-do list will help understand how the 10 points need to be spent.) You can assign a weight of 20 if there
is a lot of uncertainty around the issue and more planning is needed.

#### Boards

##### Geo Investigation Board

[This board](https://gitlab.com/groups/gitlab-org/-/boards/981056?milestone_title=No+Milestone&&label_name[]=Geo) is populated by all issues with the Geo label.

There are three columns:
- *needs investigation* : these issues may not have enough information or detail to be actively worked on. They are assigned to the team to figure out what work needs to be done and what the rough steps for development might be.
- *needs weight* : these issues have been investigated and contain enough detail to be worked on. They are assigned to the team to assess what weight needs to be applied. Please view (this) for more information about weight.
- *ready for development* : when issues have been investigated and have been weighted, they are ready to be worked on. In this state, the engineering manager and the product manager confirm that development can begin on these issues, and they are moved into the Backlog milestone.

##### Geo Backlog

[This board](https://gitlab.com/groups/gitlab-org/-/boards/981066?milestone_title=Backlog&&label_name[]=Geo) has a "ready for development" column that is populated by issues that have undergone investigation, have a weight applied, and are ready to be picked up for development. They are shown in priority order.

When work in the current milestone is complete, new issues can be picked from the top of the "ready for development" column and moved into the current milestone.

##### Current Milestone Board

This is named as "`current milestone` Geo" in the Issue Boards list.

At the start of the milestone, this will have
- the current work-in-progress issues that have come from the previous release
- deliverable items for that milestone

When there is no work left to do on this board, go to the Geo Backlog board and pick the highest priority most appropriate item to work on and assign it to the current milestone.
It will move into the Current Milestone board.

##### Geo Milestone Planning

[This board](https://gitlab.com/groups/gitlab-org/-/boards/796972?&label_name[]=Geo) is an overview of next four milestones. This is useful to see if Geo is committed to
specific work in specific releases.

##### Geo : Next Gen Geo

[This board](https://gitlab.com/groups/gitlab-org/-/boards/960355?&label_name[]=Geo&label_name[]=Geo%20Next%20Gen) contains the long running feature ideas that are referenced
from the related page in the handbook.

### Demos

The demos are recorded and should be stored in Google Drive under "GitLab Videos --> [Geo Demos](https://drive.google.com/drive/u/0/folders/1Ot2ElWwEh9vdPx1K8VO5ZMBkxlmRAXm4)".
If you recorded the demo, please make sure the recording ends up in that folder.

### Research Items

1. [Next Gen Geo](./2019-next-gen-geo.html)
