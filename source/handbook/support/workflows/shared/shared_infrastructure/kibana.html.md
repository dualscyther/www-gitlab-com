---
layout: markdown_page
title: Kibana
category: Infrastructure for troubleshooting
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Kibana

This [deck](https://docs.google.com/presentation/d/1fXFsvHvDujQ3L7uVQCiJSyxQKDhcGDQ2_PhjjhWxwx4/edit?usp=sharing) (GitLab internal only) provides an introduction to using Kibana for tracking errors in GitLab.
