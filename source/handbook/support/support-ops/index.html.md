---
layout: markdown_page
title: Support Engineer Ops
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Support Operations team maintains the day-to-day operations and software systems used by GitLab’s global Technical Support team.


## Customer Satisfaction Survey (CSAT)

At GitLab, CSAT is a measure of how satisfied our customers' are with their interaction with the GitLab Support team. This is based on survey responses from customers after each ticket is solved by the Support team using a Good or Bad rating.

The target is 95% customer satisfaction. The definition of the KPI is:

```
Satisfied [total good scores]  / Total Survey Responses [good scores + bad scores]
```

This calculation excludes cases where a survey was not offered or where it was offered but no score was provided.

The survey is sent out 24 hours after a ticket is solved. The tag `csat_survey_sent` is added to a ticket as soon as a survey is sent out. 

#### CSAT in numbers

- Our average response rate is 24%.
- In average, 90% of our tickets receive a CSAT Survey.

There are a few reasons why a customer wouldn’t receive a CSAT Survey after a ticket has been solved:

- Ticket was merged into another ticket, and subsequently closed (tag `close_by_merge`).
    - If a customer follows up on a ticket that was closed by merge, the tags of the original ticket are copied over to the new ticket, therefore not all tickets which contain this tag haven't been sent the CSAT Survey.
- Ticket was received through mail-list@gitlab.com (tickets received through this channel are automatically closed).
- Ticket was submitted by a member of the GitLab team (‘GitLab’ Organization). If a member of other GitLab Zendesk organizations submits a ticket, they will receive a survey (ie GitLab Inc., GitLab BV, GitLab Ltd).

#### Managing Negative Feedback

Starting July 9th, 2019, in order to learn about the reasons behind a bad satisfaction rating, we ask any customer who gives a negative survey response to select a reason for their dissatisfaction. This feedback is shared with the Product team and the Support Managers, depending on which reason is chosen. The dropdown options are:

- GitLab doesn't meet my needs (Product Team)
- The answer wasn't delivered in a timely manner (Support Managers)
- My issue was not resolved (Support Managers)
- The answer wasn't helpful (Support Managers)
- Some other reason

All negative and positive feedback with comment is exported to an internal repository. Once the feedback has been submitted, an Issue is created and the Support Engineering Managers are tagged when a negative rating has been submitted. It is the responsibility of the Support Engineering Managers to investigate the feedback and ensure appropriate actions are taken to either resolve the root cause of the negative experience or reduce the likelihood of it recurring.

## Other tools
{: #other-tools}

### Slack notifications

#### Premium Breach Notifications
{: #premium-breach-notifications}

The premium breach notifications is a webhook that is triggered by a ZenDesk automation titled `Premium important ticket slack notification`. The trigger looks for any
high premium tagged tickets that have less than 2 hours left on the breach clock.
The automation [runs at the top of every hour](https://support.zendesk.com/hc/en-us/articles/203662236-About-automations-and-how-they-work)
and due to this limitation, it does not run at the exact hour the ticket is less than 2 hour from breaching.

Once the ticket is updated, a trigger sends a webhook to slack which is configured on the slack side.

#### #zd-self-managed-feed and #zd-gitlab-com-feed 
{: #zd-self-managed-feed}

The `#zd-self-managed-feed` channel is updated by a webhook that's triggered whenever a Self-Managed ticket is created or updated. The Zendesk trigger is called "Slack Support Live Feed (With Organization names)"
The `#zd-self-managed-feed` channel does not display tickets addressed to GitLab.com or
GitHost.io. 

The `#zd-gitlab-com-feed` channel is updated by a webhook that's triggered whenever a GitLab.com ticket is created or updated. The Zendesk trigger is called "Slack Services Live Feed". This channel does not display EE tickets.

#### Support Bot

The [Supportbot](https://gitlab.com/gl-support/gitlab-support-bot/tree/master/) shares status of Zendesk views and number of tickets in Slack when invoked. 
