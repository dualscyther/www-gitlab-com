---
layout: markdown_page
title: "Sales Onboarding"
---

**Sales Onboarding Process**

Sales onboarding at GitLab is focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first month or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading discovery calls to begin building pipeline by or before the end of their first several weeks on the job. 

Topics covered include but are not limited to:
* Understanding the GitLab Buyer
* Understanding the Industry in Which We Compete
* Salesforce Basics
* Introduction to GitLab Sales Process & Tools
* Why GitLab and the GitLab Portfolio
* The Competition
* Critical Sales Skills & Behaviors

The process:
*  The GitLab Candidate Experience team initiates a general GitLab onboarding issue for every new GitLab team member
   - See the [People Ops onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md)
*  In the "For Sales" section of that issue, Sales Managers are instructed to 
   1.  Create a new issue in the [Sales Onboarding Issue Tracker](https://gitlab.com/gitlab-com/sales-team/sales-onboarding/issues/) using the `sales_onboarding.md` template, and 
         - Note: This should be completed within the first 2 days of a new sales team member's start date
   1.  Notify the new sales team member that this will be their sales onboarding training 
*  For an overview of the process, watch this [4.5 minute overview video](https://drive.google.com/open?id=1N56QJn7v1pUVtKoUtGZ62kfFLqJp7X0W)
*  Note: The above issue is focused on sales training; sales tool provisioning, account & territory assignments, and other Ops tasks remain in the general GitLab onboarding issue at this time in the "For Sales" section
*  Targeted roles for the sales onboarding training issue include Enterprise and Public Sector Strategic Account Leaders (SALs), Mid-Market Account Executives, SMB Customer Advocates, and Inside Sales Reps
   - Customer Success (e.g. Solution Architects, Technical Account Managers, and Professional Services Engineers) and Sales Development Rep (SDR) roles will continue to maintain their own separate CS onboarding process, but we will be collaborating and sharing best practices
