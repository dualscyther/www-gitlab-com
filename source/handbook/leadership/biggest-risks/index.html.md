---
layout: markdown_page
title: "Biggest risks"
---
## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

People frequently ask the CEO things like:

- What are the biggest risks to the company?
- What are your biggest fears?
- What keeps you up at night?

On this page, we document the biggest risks and how we intend to mitigate them.

## Lowering the hiring bar

We are growing rapidly (more than doubling headcount in CY2019 again), and there is pressure on departments to meet their [hiring targets](/handbook/hiring/charts/).
It is better for us to miss our targets than to hire people who won't be able to perform to our standards since that takes much longer to resolve.
To ensure the people we hire make the company better, we:

1. Have a standard interview structure
1. Review the interview scores of new hires to look for trends
1. [Identify and take action on underperformance](/handbook/underperformance)
1. Have the CPO and CEO sample new hires and review manager and up hires
1. Compare the external title with the new title being given
1. Conduct bar raiser interviews

## Ineffective onboarding

We are onboarding many people quickly, making it easy for things to fall behind.
Therefore we:

1. Measure the onboarding time
1. Measure the time to productivity in sales (ramp time) and engineering (time to first MR, MRs per engineer per month)
1. Make sure we work [handbook-first](/handbook/handbook-usage/#why-handbook-first), so the handbook is up to date.

## Confusion about the expected output

As we add more layers of management to accommodate the new people, it's easy to become confused about what is expected of you.

To make sure this is clear we:

1. Document who is the [DRI](/handbook/people-operations/directly-responsible-individuals/) on a decision.
1. Have clear [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis) across the company
1. Have [Key monthly reviews](/handbook/finance/operating-metrics/#key-monthly-review)
1. Have a job family that includes performance indicators
1. Have a [clear org-chart](/company/team/org-chart/) where [each individual reports to exactly one person](/handbook/leadership/#no-matrix-organization)

## Loss of the values that bind us

It's easy for a culture to get diluted if a company is growing fast.
To make our [values](/handbook/values/) stronger, we:

1. Regularly add to them and update them
1. Find new ways to [reinforce our values](/handbook/values/#how-do-we-reinforce-our-values)

## Loss of the open source community

1. Keep our [promises](/company/stewardship/#promises)
1. Keep listening
1. Assign [Merge request coaches](/job-families/expert/merge-request-coach/)
1. [Contributing organizations](/community/contributing-orgs/)

## Loss of velocity

Most companies start shipping more slowly as they grow.
To keep our pace, we need to:

1. Ensure we get 10 Merge Requests (MRs) per engineer per month
1. Have [acquired organizations](/handbook/acquisitions/) remake their functionality inside our [single application](/handbook/product/single-application/)
1. Have a [quality group](/handbook/engineering/quality/) that keeps our developer tooling efficient

## Fork and commoditize

Since we are based on an open source product, there is the risk of fork and commoditize like what [AWS experienced with ElasticSearch](https://www.youtube.com/watch?v=G6ZupYzr_Zg).

This risk is reduced, because we're application software instead of infrastructure software.
Application software is less likely to be forked and commoditized for the following reasons:

| Type of software | Application software | Infrastructure software | |
| Interface | Grafical User Interface (GUI) | Application Programming Interface (API) | A GUI is harder to commoditize than an API |
| Compute usage | Drives little compute | Drives lots of compute | Hyperclouds want to drive compute |
| Deployment | Multi-tenant (GitLab.com) | Single tenant managed service (MongDB Atlas) | Hyperclouds offer mostly managed services |
| Feature richness | Lots of proprietary features  | Few proprietary features | More proprietary features make it harder to commoditize | 
| Ecosystem activity | Lots of contributions | Few contributions | Infrastructure is more complex to contribute to. ???? |

What we need to do is:

1. [Keep up velocity](#loss-of-velocity)
1. [Keep the open source community contributing](#loss-of-the-open-source-community)
1. [Follow our buyer-based-open-core pricing model](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier)

## Competition

We will always have competition. To deal with competition, operational excellence can be a [surprisingly durable competitive advantage](https://twitter.com/patrickc/status/1090387536520728576).

We encourage operational excellence in the following ways:

1. [Efficiency value](/handbook/values/#efficiency)
1. [Long Term Profitability Targets](/handbook/finance/financial-planning-and-analysis/#long-term-profitability-targets)
1. [KPIs](/handbook/business-ops/data-team/metrics/#gitlab-kpis)
1. Open source with a lot of wider community contributors who make it easier to uncover customer demand for features and allow our organization to stay leaner.
1. A [single application](/handbook/product/single-application/) makes the user experience better, allows us to introduce new functionality to users, and it makes it easier for us to keep our velocity.
1. Run the same code for GitLab.com and and self-hosted applications and are [merging the CE and EE codebases](https://about.gitlab.com/2019/02/21/merging-ce-and-ee-codebases/)
1. How we [make decision](/handbook/leadership/#making-decisions)

## What isn't a risk

We're in a great market and have multiple waves that we're riding:

- [Digital transformation](/2019/03/19/reduce-cycle-time-digital-transformation/)
- [Cloud native and the adoption of Kubernetes](/cloud-native/)
- [Software eating the world](https://a16z.com/2011/08/20/why-software-is-eating-the-world/)
- [Customer Experience](/handbook/customer-success/vision/)
- [DevOps](/devops)
- [DevOps tooling consolidation](https://devops.com/challenges-devops-standardization/)
- [Microservices](/topics/microservices/)
- [Progressive delivery](https://redmonk.com/jgovernor/2018/08/06/towards-progressive-delivery/)
- [Open source](/20-years-open-source/)
- [Workloads moving to the cloud](https://www.synopsys.com/blogs/software-security/cloud-migration-business/)
- [All remote](https://about.gitlab.com/company/culture/all-remote/)
