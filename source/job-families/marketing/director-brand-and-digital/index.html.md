---
layout:job_family_page
title: "Director of Brand and Digital"
---

Our senior design leader has a deep digital experience to lead the GitLab brand and digital experience team.  Reporting to the CMO, the Director of Brand and Digital Experience is accountable for evolving the GitLab brand to support the aggressive growth of the company, in partnership with Corporate Marketing. This is balanced by a deep understanding of the brand history, the importance of the brand to the open source community, and the impact of the brand on the product itself.  The Director manages a growing team of brand designers, with the dual purpose of evolving the brand and supporting the day-to-day production design needs of the company (such as events, marketing campaigns and swag).  Additionally, the Director of Brand and Digital Experience manages and grows a digital experience team, with both digital designers and full-stack web developers, and is accountable for delivering conversion-focused web properties, landing pages, email templates, digital ads and digital experiences, working in partnership with the marketing program managers and content team to deliver on marketing objectives.  Finally, the Director forges a strong partnership with the GitLab UX team, to align on brand and design objectives with the product organization through the vehicle of the Pajamas design system (https://design.gitlab.com/).

The Director of Brand & Digital has experience leading both small and large teams of designers and developers in both in-house design organizations and, ideally, agencies. He or she has a strong portfolio of digital experience work, focused on conversion-focused web properties and design assets, as well as strong experience on global brand projects. He or she demonstrates an ability to work in a fast-paced environment with tight deadlines, supporting a global marketing team. Ideally, but not required, the candidate will have had experience with developing flat file websites and some knowledge of JAMstack.

## Responsibilities

*   Lead a brand design team, focused on delivering brand assets in support of company marketing activities and events, and on evolving the overall brand of GitLab
*   Lead a digital experience team, focused on delivering website pages, landing pages, email templates, digital ads and digital experiences in pursuit of marketing objectives
*   Be a visible brand and design leader at GitLab, partnering closely with the UX team to evolve the overall brand and design of GitLab through the [Pajamas design system](https://design.gitlab.com/)
*   Partner closely with marketing peers, to align to marketing objectives and prioritize brand and digital experience work
*   Hire and mentor designers and full-stack web developers with an eye towards career growth for the team
*   Consult with marketing leaders on effective digital experience strategy for marketing programs
*   Own digital conversion for the marketing organization, reporting on conversion improvements and website traffic improvements based on changes implemented by the team
*   Partner with Digital Marketing Programs to implement an every-day SEO strategy, seeking to iterate constantly to improve conversion and web traffic to about.gitlab.com
*   Be the public maintainer of about.gitlab.com

## Requirements

*   10-15 years of experience managing design teams, ideally in both in-house and agency environments
*   Demonstrated portfolio of conversion-focused digital assets that can be shared during the interview process
*   Experience with modern digital technologies, including Adobe, marketing automation systems (like Marketo, Eloqua, Hubspot, etc.), and CMSs
*   Experience with modern web coding frameworks, such as CSS, JS, boot-strap, etc.
*   Experience with high-tech companies and/or open source companies and communities is a plus
*   An established track record of hiring and growing design and web development talent
*   Design degree or documented formal design training